# README #

### How do I get set up and running? ###

* This requires python requests module
* Run as shown, date format is dd.mm.yyyy
~~~
whoever@whatever$ python pm10.py 11.1.2017
 
------------------------------------
         Kraków    11.1.2017
------------------------------------
place                   1-16  12-22
------------------------------------
Aleja Krasińskiego     308.17 321.81
Kraków, os. Piastów    264.70 318.24
Kraków, ul. Złoty Róg  242.43 258.99
Kraków-Kurdwanów       314.79 291.04
Kraków-ul. Dietla      323.62 380.34
Nowa Huta              331.30 324.22
Kraków, os. Wadów      280.99 298.46
Kraków, ul. Telimeny   322.10 291.80
------------------------------------
average                298.51 310.61

~~~

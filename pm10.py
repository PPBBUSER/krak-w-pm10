#!/usr/bin/python
# -*- coding: UTF-8 -*-
import requests
import time
import sys

URL="http://monitoring.krakow.pios.gov.pl/dane-pomiarowe/pobierz"

payload={}
queryJson='{"measType":"Auto","viewType":"Parameter","dateRange":"Day","date":"%s","viewTypeEntityId":"pm10","channels":[46,1747,1752,148,1723,57,1921,1914]}'

headers={'Accept': 'application/json, text/javascript, */*; q=0.01',
'Accept-Encoding': None,
'Origin': 'http://monitoring.krakow.pios.gov.pl',
'X-Requested-With': 'XMLHttpRequest',
'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
'DNT': '1'}


### 1 do 16 oraz od 12 do 22  >150ug na wszystkich stacjach

def epochTimeFromDateAndHour(date, hour):
  date_string = date+" %i:00"%(hour)
  return int(time.mktime(time.strptime(date_string, "%d.%m.%Y %H:%M")))

def average(data):
  if len(data)==0:
    return 0
  s=0.0
  for d in data:
    s = s + d[0]  
  return s/len(data)

def main():
  if len(sys.argv) >1:
    date = sys.argv[1]
    try:
      time.strptime(date, '%d.%m.%Y')
    except ValueError:
      print "Incorrect data format, should be dd.mm.yyyy (example: 1.11.2001), got: {}".format(date)
      return
  else:
    date = "22.06.2016"
  
  range1_low  =  epochTimeFromDateAndHour(date,1)
  range1_high =  epochTimeFromDateAndHour(date,17)
  range2_low  =  epochTimeFromDateAndHour(date,12)
  range2_high =  epochTimeFromDateAndHour(date,23)
 
  payload['query'] = queryJson%(date)
  r = requests.post(URL,headers=headers,data=payload)
  json_response = r.json()
# print len(r.content)
  complete_data=[]
  if json_response['success']:
# data->series[]
     average1=0.0
     average2=0.0
     c1=0 #counts
     c2=0
 
     print u"-"*44
     print u"             Kraków    "+date
     print u"-"*44
     print u"place                     1-16      12-22"
     print u"-"*44
     for serie in json_response['data']['series']:
       range1=[]
       range2=[] 
       label = serie['label']
       for data in serie['data']:
         timestamp = int(data[0])
         value = float(data[1])
         if (timestamp >= range1_low and timestamp < range1_high):
           range1.append((value,timestamp))           
         if (timestamp >= range2_low and timestamp < range2_high):
           range2.append((value,timestamp))
       #print range1, range2, average(range1), average(range2)
       complete_data.append((label,range1,range2))
       ar1 = average(range1)
       ar2 = average(range2)
      
       if ( ar1 > 0 ): 
         average1 = average1 + average(range1)
         c1 = c1 + 1
       if ( ar2 > 0 ):
         average2 = average2 + average(range2)
         c2 = c2 + 1
       print u"{:22} {:3.2f}({:2d}) {:3.2f}({:2d})".format(label,average(range1),len(range1), average(range2),len(range2))
     print u"-"*44
     if (c1*c2 > 0):
       print u"{:22} {:3.2f}     {:3.2f}".format("average",average1/c1, average2/c2)
     elif (c1 == 0) & (c2 >0):
       print u"{:22} -----     {:3.2f}".format("average", average2/c2)
     elif (c2 == 0) & (c1 > 0):
       print u"{:22} {:3.2f}     -----".format("average", average1/c1)
     else:
       print u"{:22} -----     -----".format("average")
    
#  print complete_data



if __name__ == "__main__":
  main()


